﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="SOs/AI/EnemyPerceptionSettings", fileName ="EnemyPerceptionSetting")]
public class EnemyPerceptionSettings : ScriptableObject {

    [SerializeField] private float frontRangeFar = 20.0f;
    [SerializeField] private float frontAngleFar = 30.0f;
    [SerializeField] private float frontRangeClose = 10.0f;
    [SerializeField] private float frontAngleClose = 90.0f;
    [SerializeField] private float closeRange = 2.0f;
    [SerializeField] private float checkInterval = 0.4f;
    
    [SerializeField] private LayerMask mask;

    public float FrontRangeFar { get { return frontRangeFar; } }
    public float FrontAngleFar { get { return frontAngleFar; } }
    public float FrontRangeClose { get { return frontRangeClose; } }
    public float FrontAngleClose { get { return frontAngleClose; } }
    public float CloseRange { get { return closeRange; } }
    public float CheckInterval { get { return checkInterval; } }
    public LayerMask Mask { get { return mask; } }
    
}
