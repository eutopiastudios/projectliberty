﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerUseItem : MonoBehaviour
{
    public TMP_Text m_tooltip;

    public Transform m_raycastOrigin;
    public LayerMask m_raycastLayers;
    public float m_raycastDistance;

    private RaycastHit m_hitInfo;

    void Update()
    {
        if(Physics.Raycast(m_raycastOrigin.position, m_raycastOrigin.transform.forward, out m_hitInfo, m_raycastDistance, m_raycastLayers))
        {
            UseableItem item;

            if (item = m_hitInfo.transform.GetComponent<UseableItem>())
            {
                m_tooltip.enabled = true;
                m_tooltip.text = item.m_tooltip;

                if(Input.GetButtonDown("Interact"))
                {
                    item.Use(transform.position);
                }
            }
        }
        else
        {
            m_tooltip.enabled = false;
        }
    }
}
