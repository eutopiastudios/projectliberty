﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// this should actually be something that goes on characters, need to investigate how to use it...
// will probably replace with scriptable object asset or sth
public interface IEnemyPerceptionInput {

    bool IsInvisible();
    void ReceiveDamage(int amount);
    void PowerUp(int _energy, int _health);
    void Save(string levelName);
    bool CanUseEnergy();
    bool CanUseHealth();
}
