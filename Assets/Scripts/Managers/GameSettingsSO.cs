﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Managers/Settings")]
[Serializable]
public class GameSettingsSO : ScriptableObject {

    [Header("Debug")]
    public bool debugMode;

    public GraphicsQuality graphicsQuality;
    public float masterVolume;
    public Language language;



    //public GraphicsQuality GraphicsQuality { get { return graphicsQuality; } set { graphicsQuality = value; } }
    //public float MasterVolume { get { return masterVolume; } set { masterVolume = value; } }
    //public Language Language { get { return language; } set { language = value; } }

}
