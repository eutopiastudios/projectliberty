﻿using UnityEngine;

public class Settings : MonoBehaviour {

    public GameSettingsSO _settings; // drag GameSettings asset here in inspector

    [SerializeField]
    public static GameSettingsSO settings;
    public static Settings s;


    //Player Prefs or Scriptable Object Asset?!

    void Awake()
    {
        //DontDestroyOnLoad(gameObject);
        if (Settings.s == null)
        {
            Settings.s = this;
        }
        else
        {
            Debug.LogWarning("A previously awakened Settings MonoBehaviour exists!", gameObject);
        }
        if (Settings.settings == null)
        {
            Settings.settings = _settings;
        }
        LoadData();
    }
    private void Start()
    {
        QualitySettings.SetQualityLevel((int)settings.graphicsQuality);

    }

    private void LoadData()
    {
        SaveDataSO.LoadFromFile(settings, settings.ToString());
    }
    private void SaveData()
    {
        SaveDataSO.SaveToFile(settings, settings.ToString());

    }
 
    private void GetGlobalPlayerPrefs()
    {
        settings.masterVolume = PlayerPreferences.GetVolumePref();
        AudioListener.volume = settings.masterVolume;
        settings.graphicsQuality = PlayerPreferences.GetGraphicsPref();
        QualitySettings.SetQualityLevel((int)settings.graphicsQuality);
    }

    private void SetPlayerPrefs()
    {
        PlayerPreferences.SetVolumePref(settings.masterVolume);
        PlayerPreferences.SetGraphicsPref(settings.graphicsQuality);
        PlayerPreferences.SavePlayerPrefs();
    }

    private void OnDisable()
    {
        SaveData();
    }

    private void OnApplicationQuit()
    {
        SaveData();
        SetPlayerPrefs();
    }

    /// CHANGE SETTINGS
    public void ChangeQualitySettings()
    {
        QualitySettings.SetQualityLevel((int) settings.graphicsQuality);
        PlayerPreferences.SetGraphicsPref(settings.graphicsQuality);
    }

    public void ChangeVolumeLevel()
    {
        AudioListener.volume = settings.masterVolume;
        PlayerPreferences.SetVolumePref(settings.masterVolume);
    }
}
