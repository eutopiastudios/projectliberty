﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPreferences {

    private static string volume = "Volume";
    private static string graphicsQ = "GraphicsQuality";


    public static void SavePlayerPrefs()
    {
        PlayerPrefs.Save();
    }

    public static void HardResetAllPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }


    public static void SetVolumePref(float masterVolume)
    {
        PlayerPrefs.SetFloat(volume, masterVolume);
        PlayerPrefs.Save();
    }

    public static float GetVolumePref()
    {
        return PlayerPrefs.GetFloat(volume, 1f);
    }

    public static void SetGraphicsPref(GraphicsQuality quality)
    {
        PlayerPrefs.SetInt(graphicsQ, (int)quality);
        PlayerPrefs.Save();
    }

    public static GraphicsQuality GetGraphicsPref()
    {
        return (GraphicsQuality)PlayerPrefs.GetInt(graphicsQ, 1);
    }
}
