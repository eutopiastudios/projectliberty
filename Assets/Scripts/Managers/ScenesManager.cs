﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour {


    public string activeScene; 


	// Use this for initialization
	void Start () {
        Invoke("ChangeScene", 5f);
	}

    public void ChangeScene(string sceneName, float timeUntilChange)
    {
        activeScene = SceneManager.GetActiveScene().name;
    }
 

    public void UnloadScene(string OldsceneName)
    {
        Debug.Log("Removing Scene " + OldsceneName);
        SceneManager.UnloadSceneAsync(OldsceneName);
    }
}
