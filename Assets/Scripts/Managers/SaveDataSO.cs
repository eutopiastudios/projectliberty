﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public static class SaveDataSO  {


    //[Header("Meta")]
    //public string savedDataName;
    //[Header("ScriptableObjects")]
    //public List<ScriptableObject> objectsToSave;

    //private void OnEnable()
    //{
    //for (int i = 0; i < objectsToSave.Count; i++)
    //{
    //    if (File.Exists(Application.persistentDataPath + string.Format("/{0}_{1}.pso", savedDataName, i)))
    //    {
    //        BinaryFormatter bf = new BinaryFormatter();
    //        FileStream file = File.Open(Application.persistentDataPath + string.Format("/{0}_{1}.pso", savedDataName, i), FileMode.Open);
    //        JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), objectsToSave[i]);
    //        file.Close();
    //    }
    //}
    //}

    public static void LoadFromFile(ScriptableObject objectToSave, string savedDataName)
    {

        if (File.Exists(Application.persistentDataPath + string.Format("/{0}.pso", savedDataName)))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + string.Format("/{0}.pso", savedDataName), FileMode.Open);
            JsonUtility.FromJsonOverwrite((string)bf.Deserialize(file), objectToSave);
            file.Close();
        }
        
    }

    //private void OnDisable()
    //{
 
    //}

    public static void SaveToFile(ScriptableObject objectToSave, string savedDataName)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + string.Format("/{0}.pso", savedDataName));
        var json = JsonUtility.ToJson(objectToSave);
        bf.Serialize(file, json);
        file.Close();
    }
}
