﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Action/Flee")]
public class FleeAction : Action {

    public override void Act(StateController controller)
    {
        Flee(controller);
    }

    private void Flee(StateController controller)
    {
        Vector3 dir = controller.transform.position - controller.target.position;
        controller.anim.SetTrigger("Run");
        controller.navMeshAgent.SetDestination(controller.transform.position + dir.normalized * 10);
        controller.navMeshAgent.isStopped = false;
    }
}
