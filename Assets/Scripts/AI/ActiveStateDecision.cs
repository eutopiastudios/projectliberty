﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decision/ActiveStateDecision")]

public class ActiveStateDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool targetActive = controller.target.gameObject.activeSelf;
        return targetActive;
    }
}
