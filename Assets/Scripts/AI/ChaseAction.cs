﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Action/Chase")]

public class ChaseAction : Action
{

    public override void Act(StateController controller)
    {
        Chase(controller);
    }

    private void Chase(StateController controller)
    {
        controller.navMeshAgent.isStopped = false;
        controller.anim.SetTrigger("Run");
        controller.navMeshAgent.SetDestination(controller.target.position);
    }
}
