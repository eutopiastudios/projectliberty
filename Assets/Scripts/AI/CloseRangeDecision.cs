﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="PluggableAI/Decision/CloseRange")]
public class CloseRange : Decision {


    private EnemyPerceptionSettings enemyPerceptiponSettings;
    private float closeRange;
    private float checkInterval;
    private LayerMask mask;
    bool inCloseRange = false;


    public override bool Decide(StateController controller)
    {
        //bool seeTarget = LookForTarget(controller);
        bool seeTarget = CheckDistance(controller);
        return seeTarget;
    }

    private bool CheckDistance(StateController controller)
    {
        bool inRange = false;
        float distance = Vector3.Distance(controller.transform.position, controller.target.position);

        inRange = distance < controller.enemyPerceptionSettings.CloseRange;
        return inRange;
    }

    //private bool LookForTarget(StateController controller)
    //{
    //    enemyPerceptiponSettings = controller.enemyPerceptionSettings;

    //    closeRange = enemyPerceptiponSettings.CloseRange;
    //    checkInterval = enemyPerceptiponSettings.CheckInterval;
    //    mask = enemyPerceptiponSettings.Mask;

    //    return GetObjectsInRange(controller);
    //}

    //private bool GetObjectsInRange(StateController controller)
    //{
    //    inCloseRange = false;
    //    Collider[] hitColliders = Physics.OverlapSphere(controller.transform.position, closeRange, mask);

    //    for (int i = 0; i < hitColliders.Length; i++)
    //    {
    //        CheckLineOfSight(controller, hitColliders[i]);
    //    }
    //    return inCloseRange;
    //}

    //private bool CheckLineOfSight(StateController controller, Collider c)
    //{
    //    RaycastHit hit;
    //    float range = -1;
    //    bool inRange = false;
    //    inCloseRange = false;
    //    // We Linecast from this agent to PC to see if there is a line of sight.
    //    if (Physics.Linecast(controller.transform.position, c.transform.position, out hit))
    //    {
    //        if (hit.transform == c.transform)
    //        {
    //            if (hit.distance <= closeRange)
    //            {
    //                inRange = true;
    //                inCloseRange = true;
    //                range = hit.distance;
    //                controller.target = c.transform;
    //            }
    //            else
    //            {
    //                inCloseRange = false;
    //            }
    //        }
    //    }
    //    return inCloseRange;
    //}
}
