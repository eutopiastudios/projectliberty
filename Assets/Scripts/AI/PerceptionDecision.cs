﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName ="PluggableAI/Decision/Perceive")]

public class PerceptionDecision : Decision {

    private float frontRangeFar;
    // if angle between forward and los is less than this, the target is at front
    private float frontAngleFar;
    private float frontRangeClose;
    private float frontAngleClose;
    private float closeRange;
    private float checkInterval;
    // all object in the given layers are perceived
    private LayerMask mask;
    private EnemyPerceptionSettings enemyPerceptionSettings;
    private bool inRange = false;
    private bool withinCloseRange = false;


    public override bool Decide(StateController controller)
    {
        bool seeTarget = LookForTarget(controller);
        return seeTarget;
    }

    private bool LookForTarget(StateController controller)
    {
        enemyPerceptionSettings = controller.enemyPerceptionSettings;

        frontRangeFar = enemyPerceptionSettings.FrontRangeFar;
        frontAngleFar = enemyPerceptionSettings.FrontAngleFar;
        frontRangeClose = enemyPerceptionSettings.FrontRangeClose;
        frontAngleClose = enemyPerceptionSettings.FrontAngleClose;
        closeRange = enemyPerceptionSettings.CloseRange;
        checkInterval = enemyPerceptionSettings.CheckInterval;
        mask = enemyPerceptionSettings.Mask;

        return GetObjectsInRange(controller);   
    }

    private bool GetObjectsInRange(StateController controller)
    {
        // Clearing data from previous time
        //perceived.Clear();
        inRange = false;
        // Get all objects that are within the maximum perception range (frontRange)
        // in the layers defined by mask. 
        Collider[] hitColliders = Physics.OverlapSphere(controller.transform.position, frontRangeFar, mask);

        for (int i = 0; i < hitColliders.Length; i++)
        {
            CheckLineOfSight(controller, hitColliders[i]);
        }
        return inRange;
    }

    private bool CheckLineOfSight(StateController controller, Collider c)
    {
        RaycastHit hit;
        float range = -1;

        if (Physics.Linecast(controller.transform.position, c.transform.position, out hit))
        {
            if (hit.transform == c.transform)
            {
                if (hit.distance <= frontRangeClose)
                {
                    if (Vector3.Angle(controller.transform.forward, hit.transform.position - controller.transform.position) <= frontAngleClose)
                    {
                        inRange = true;
                        range = hit.distance;
                        controller.target = c.transform;
                    }
           
                }
                else if (hit.distance <= frontRangeFar)
                {
                    if (Vector3.Angle(controller.transform.forward, hit.transform.position - controller.transform.position) <= frontAngleFar)
                    {
                        inRange = true;
                        range = hit.distance;
                        controller.target = c.transform;
                    }
           
                }
            }
        }
        return inRange;
    }
}
