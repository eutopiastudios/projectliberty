﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName ="PluggableAI/Decision/Range")]

public class RangeDecision : Decision {

    private float frontRangeFar;
    // if angle between forward and los is less than this, the target is at front
    private float frontAngleFar;
    private float frontRangeClose;
    private float frontAngleClose;
    private float closeRange;
    private float checkInterval;
    // all object in the given layers are perceived
    private LayerMask mask;
    private EnemyPerceptionSettings enemyPerceptionSettings;
    private bool inRange = false;
    private bool withinCloseRange = false;


    // check if distance within shooting perception (farRange), but not within close combat;

    public override bool Decide(StateController controller)
    {
        bool seeTarget = LookForTarget(controller);
        //bool seeTarget = CheckDistance(controller);
        return seeTarget;
    }

    //private bool CheckDistance(StateController controller)
    //{
    //    bool inRange = false;
    //    float distance = Vector3.Distance(controller.transform.position, controller.target.position);

    //    inRange = distance > controller.enemyPerceptionSettings.CloseRange && distance < controller.enemyPerceptionSettings.FrontRangeFar;
    //    return inRange;
    //}

    private bool LookForTarget(StateController controller)
    {
        enemyPerceptionSettings = controller.enemyPerceptionSettings;

        frontRangeFar = enemyPerceptionSettings.FrontRangeFar;
        frontAngleFar = enemyPerceptionSettings.FrontAngleFar;
        frontRangeClose = enemyPerceptionSettings.FrontRangeClose;
        frontAngleClose = enemyPerceptionSettings.FrontAngleClose;
        closeRange = enemyPerceptionSettings.CloseRange;
        checkInterval = enemyPerceptionSettings.CheckInterval;
        mask = enemyPerceptionSettings.Mask;

        return CheckLineOfSight(controller, controller.target);
    }

    private bool CheckLineOfSight(StateController controller, Transform c)
    {
        RaycastHit hit;

        if (Physics.Linecast(controller.transform.position, c.transform.position, out hit))
        {
            if (hit.transform == c.transform)
            {

                if (hit.distance <= closeRange)
                {
                    inRange = true;
                }
                else if (hit.distance <= frontRangeClose)
                {
                    if (Vector3.Angle(controller.transform.forward, hit.transform.position - controller.transform.position) <= frontAngleClose)
                    {
                        inRange = true;
                    }
                }
                else if (hit.distance <= frontRangeFar)
                {
                    if (Vector3.Angle(controller.transform.forward, hit.transform.position - controller.transform.position) <= frontAngleFar)
                    {
                        inRange = true;
                    }
                }
                else
                {
                    inRange = false;
                }
            }
        }
        return inRange;
    }
}
