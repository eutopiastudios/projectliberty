﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decision/Health")]
public class LowHealthDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool seeTarget = CheckHealth(controller);
        //bool seeTarget = CheckDistance(controller);
        return seeTarget;
    }

    private bool CheckHealth(StateController controller)
    {
        bool lowHealth = controller.enemyHealth.healthSettings.health < 10;
        return lowHealth;
    }
}
