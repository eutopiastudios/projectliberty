﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Action/RangedAttack")]

public class RangedAttackAction : Action {

    public override void Act(StateController controller)
    {
        FireAttack(controller);
    }

    private void FireAttack(StateController controller)
    {
        controller.navMeshAgent.isStopped = true;
        controller.anim.SetTrigger("Shoot");
        controller.enemyAttack.RangedAttack();
    }
}
