﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decision/WithinRange")]
public class WithinRangeDecision : Decision
{


    private EnemyPerceptionSettings enemyPerceptiponSettings;
    private float farRange;
    private float checkInterval;
    private LayerMask mask;
    bool withinRange = false;


    public override bool Decide(StateController controller)
    {
        bool withinRange = LookForTarget(controller);
        return withinRange;
    }

    private bool LookForTarget(StateController controller)
    {
        enemyPerceptiponSettings = controller.enemyPerceptionSettings;

        farRange = enemyPerceptiponSettings.FrontRangeFar;
        checkInterval = enemyPerceptiponSettings.CheckInterval;
        mask = enemyPerceptiponSettings.Mask;

        bool inRange = Vector3.Distance(controller.transform.position, controller.target.position) < farRange;
        return inRange;
    }
}