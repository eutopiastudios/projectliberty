﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


[CreateAssetMenu(menuName ="PluggableAI/Action/Patrol")]
public class PatrolAction : Action {

	public override void Act(StateController controller)
    {
        Patrol(controller);
    }

    private void Patrol(StateController controller)
    {
        controller.navMeshAgent.SetDestination(controller.wayPointList[controller.nextWaypoint].position);
        controller.navMeshAgent.isStopped = false;
        controller.anim.SetTrigger("Run");

        if (controller.navMeshAgent.remainingDistance <= controller.navMeshAgent.stoppingDistance && !controller.navMeshAgent.pathPending)
        {
            controller.nextWaypoint = (controller.nextWaypoint + 1) % controller.wayPointList.Count;
        }
    }
}
