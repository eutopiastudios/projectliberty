﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



// check whether target is within chasable range, if not NPC loses track of target

[CreateAssetMenu(menuName = "PluggableAI/Decision/OutOfSight")]
public class OutOfSightDecision : Decision {

    public override bool Decide(StateController controller)
    {
        bool seeTarget = CheckDistance(controller);
        return seeTarget;
    }

    private bool CheckDistance(StateController controller)
    {

        bool outOfSight = false;
        float distance = Vector3.Distance(controller.transform.position, controller.target.position);
        outOfSight = distance > controller.enemyAttackSettings.ShootingDistance;
        return outOfSight;
    }
}
