﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateController : MonoBehaviour {

    public State currentState;
	//public EnemyStats enemyStats;
	public Transform eyes;


	[HideInInspector] public NavMeshAgent navMeshAgent;
    //[HideInInspector] public Complete.TankShooting tankShooting;
    [HideInInspector]public EnemyAttack enemyAttack;
    [HideInInspector] public EnemyHealth enemyHealth;
     public List<Transform> wayPointList;
    [HideInInspector] public int nextWaypoint;
    [HideInInspector] public Transform target;
    [HideInInspector] public Animator anim;

	private bool aiActive = true;

    [SerializeField] public EnemyPerceptionSettings enemyPerceptionSettings;
    [SerializeField] public EnemyAttackSettings enemyAttackSettings;
    public State remainState;

    [HideInInspector] public float startTime = 0;

    public float distance;
    private Transform player;

    void Awake () 
	{
        //tankShooting = GetComponent<Complete.TankShooting> ();
        enemyAttack = GetComponent<EnemyAttack>();
        enemyHealth = GetComponent<EnemyHealth>();
		navMeshAgent = GetComponent<NavMeshAgent> ();
        anim = GetComponent<Animator>();

        player = GameObject.FindGameObjectWithTag("Player").transform;

        /////
        target = player;
	}

    private void Start()
    {
        InvokeRepeating("CheckState", 1f, 1f);
    }

    private void CheckState()
    {
        if (!aiActive) { return; }

        currentState.ChangeState(this);
        distance = Vector3.Distance(transform.position, player.position);
    }

    private void Update()
    {
        //if (!aiActive) { return; }

        //currentState.ChangeState(this);

        //print(currentState);
        //distance = Vector3.Distance(transform.position, player.position);
    }

    public void SetupAI(bool aiActivationFromTankManager, List<Transform> wayPointsFromTankManager)
    {
        //wayPointList = wayPointsFromTankManager;
        //aiActive = aiActivationFromTankManager;
        if (aiActive)
        {
            navMeshAgent.enabled = true;
        }
        else
        {
            navMeshAgent.enabled = false;
        }
    }

    public void TransitionToState(State nextState)
    {
        if (nextState != remainState)
        {
            currentState = nextState;
        }
    }

    public bool CountdownElapsed(float duration)
    {
        startTime += Time.deltaTime;
        print(startTime >= duration);
        return (startTime >= duration);
    }
}