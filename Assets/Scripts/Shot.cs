﻿using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour
{

    public float damage = 1;
    public bool isEnemy = false;
    public Vector3 direction = new Vector3(0f, 0f, 1f);
    public Vector3 speed = new Vector3(25f, 25f, 25f);
    private WaitForSeconds delay = new WaitForSeconds(15);


    private void OnEnable()
    {
        StartCoroutine(SetInactive());
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 movement = new Vector3(speed.x * direction.x, speed.y * direction.y, speed.z * direction.z);
        movement *= Time.deltaTime;
        transform.Translate(movement, Space.World);
      //  Debug.DrawRay(transform.position, movement * 100, Color.red);
    }

    void OnCollisionEnter(Collision col)
    {

        gameObject.SetActive(false);
        
    }

    private IEnumerator SetInactive()
    {
        yield return delay;
        gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.velocity = Vector3.zero;
        rb.velocity = new Vector3(0f, 0f, 0f);
        rb.angularVelocity = new Vector3(0f, 0f, 0f);
        rb.rotation = Quaternion.Euler(Vector3.zero);
    }
}
