﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateController : MonoBehaviour {

    private Animator anim;
    private EnemyPerception enemyPcptn;
    public EnemyStates currState = EnemyStates.Idle;

    public bool isSuspicious = false;
    public bool isInRange = false;
    public bool FightsRanged = false;


    private void Update()
    {
        print(currState);   
    }

    private void Awake()
    {
        enemyPcptn = GetComponent<EnemyPerception>();
        anim = GetComponent<Animator>();

        enemyPcptn.OnStateChange += ChangeStateTo;
    }

    public void ChangeStateTo(object source, StateChangedArgs e)
    {
        currState = e.enemyState;
        SetState();
    }

    void ChangeState(EnemyStates state)
    {
        currState = state;
        SetState();
    }

    private void SetState()
    {
        switch (currState)
        {
            case EnemyStates.Idle:
                RunIdleNode();
                break;
            case EnemyStates.Guard:
                Guard();               
                break;
            case EnemyStates.Combat:
                Combat();
                break;
        }
    }

    void RunIdleNode()
    {
        Idle();
    }

    void RunGuardNode()
    {
        Guard();
    }

    void RunCombatNode()
    {
        Combat();
    }

    void Idle()
    {
        anim.SetTrigger("Idle");
    }

    void Guard()
    {
        if (isSuspicious)
            { SearchForTarget(); }
        else
            { Patrol(); }
    }

    void Combat()
    {
        if (enemyPcptn.withinPerceptionRange)
        {
            if (enemyPcptn.withinCloseRange)
                { MeleeAttack(); }
            else
                { RangedAttack(); }
        }
        else
            { SearchForTarget(); }
    }

    void SearchForTarget()
    {
    }
    void Patrol()
    {
        GetComponent<EnemyAIMovement>().Patrol();
        anim.SetTrigger("Run");
    }

    void RangedAttack()
    {
        GetComponent<EnemyAttack>().RangedAttack();
        anim.SetTrigger("Shoot");
    }

    void MeleeAttack()
    {
    }
}
