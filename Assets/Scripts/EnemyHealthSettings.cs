﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SOs/AI/EnemyHealthSettings", fileName = "EnemyHealthSetting")]

public class EnemyHealthSettings : ScriptableObject
{
    [SerializeField] public float health = 50;
}