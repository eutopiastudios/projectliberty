﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SOs/AI/EnemyAttackSettings", fileName = "EnemyAttackSetting")]

public class EnemyAttackSettings : ScriptableObject
{

    [SerializeField] private float shootingDistance = 50;
    [SerializeField] private float shotInterval = 5;
    [SerializeField] private GameObject bullet;
    //[SerializeField] private string bulletTag;

    public float ShootingDistance { get { return shootingDistance; } }
    public float ShotInterval { get { return shotInterval; } }
    //public GameObject Bullet { get { return bullet; } }
    public string Bullet { get { return bullet.tag; } }


    public void ShootAt(Transform enemy, Transform target)
    {
        bullet.GetComponent<Shot>().direction = (target.position - enemy.position).normalized;

        //// Randomize bullet movement so they shoot like Stormtroopers!
        //if (Random.Range(0, 2) == 0)
        //{
        //    bullet.GetComponent<Shot>().direction += new Vector3(Random.Range(-0.015f, 0.015f), Random.Range(-0.02f, 0.02f), Random.Range(-0.05f, 0.05f));
        //}
        Quaternion bulletRotation = Quaternion.LookRotation(bullet.GetComponent<Shot>().direction);
        bullet.transform.rotation = bulletRotation;
    }

}
