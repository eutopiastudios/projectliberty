﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollTexture : MonoBehaviour {

	[Header("Finds texture on object and moves by X and Y")]
	public float ScrollSpeedX = 0f;
	public float ScrollSpeedY = 0f;	

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		float OffSetX = Time.time * ScrollSpeedX;
		float OffSetY = Time.time * ScrollSpeedY;

		GetComponent<Renderer>().material.mainTextureOffset = new Vector2(OffSetX, OffSetY);
	}
}
