﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPerception : MonoBehaviour
{

    private float frontRangeFar;
    // if angle between forward and los is less than this, the target is at front
    private float frontAngleFar;
    private float frontRangeClose;
    private float frontAngleClose;
    private float closeRange;
    private float checkInterval;
    // all object in the given layers are perceived
    private LayerMask mask;
    [SerializeField] private EnemyPerceptionSettings enemyPerceptiponSettings;
    public bool withinPerceptionRange = false;
    public bool withinCloseRange = false;

    //private ArrayList perceived;

    private EnemyStateController stateCtrl;
    //private EnemyStates currState = EnemyStates.Idle;

    public delegate void ChangedState(object sender, StateChangedArgs args);
    public event ChangedState OnStateChange;

    protected virtual void OnStateChanged(StateChangedArgs e)
    {
        if (OnStateChange != null) OnStateChange(this, e);
    }

    private void Awake()
    {
        frontRangeFar = enemyPerceptiponSettings.FrontRangeFar;
        frontAngleFar = enemyPerceptiponSettings.FrontAngleFar;
        frontRangeClose = enemyPerceptiponSettings.FrontRangeClose;
        frontAngleClose = enemyPerceptiponSettings.FrontAngleClose;
        closeRange = enemyPerceptiponSettings.CloseRange;
        checkInterval = enemyPerceptiponSettings.CheckInterval;
        mask = enemyPerceptiponSettings.Mask;

        stateCtrl = GetComponent<EnemyStateController>();
    }

    void Start()
    {
        withinPerceptionRange = false;
        // We check what is seen in every checkInterval seconds.
       // perceived = new ArrayList();
        InvokeRepeating("GetObjectsInRange", checkInterval, checkInterval);
    }

    private void GetObjectsInRange()
    {
        // Clearing data from previous time
        //perceived.Clear();
        withinPerceptionRange = false;
        // Get all objects that are within the maximum perception range (frontRange)
        // in the layers defined by mask. 
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, frontRangeFar, mask);
        if (hitColliders.Length == 0)
        {
            ChangeState(EnemyStates.Guard);
        }

        for (int i = 0; i < hitColliders.Length; i++)
        {
            CheckLineOfSight(hitColliders[i]);
        }
    }

    private void CheckLineOfSight(Collider c)
    {
        RaycastHit hit;
        float range = -1;
        bool inRange = false;
        withinCloseRange = false;
        // We Linecast from this agent to PC to see if there is a line of sight.
        if (Physics.Linecast(transform.position, c.transform.position, out hit))
        {
            if (hit.transform == c.transform)
            {
                if (hit.distance <= closeRange)
                {
                    inRange = true;
                    withinCloseRange = true;
                    range = hit.distance;
                    print("Melee attack placeholder");
                }
                else if (hit.distance <= frontRangeClose)
                {
                    if (Vector3.Angle(transform.forward, hit.transform.position - transform.position) <= frontAngleClose)
                    {
                        inRange = true;
                        range = hit.distance;
                    }
                }
                else if (hit.distance <= frontRangeFar)
                {
                    if (Vector3.Angle(transform.forward, hit.transform.position - transform.position) <= frontAngleFar)
                    {
                        inRange = true;
                        range = hit.distance;
                    }
                }
            }
        }
        if (inRange)
        {
            withinPerceptionRange = true;
            ChangeState(EnemyStates.Combat);

            //IEnemyPerceptionInput character = c.gameObject.GetComponent<IEnemyPerceptionInput>();
            //if (character != null)
            //{
            //    perceived.Add(new PerceivedObjectData(character, range, c.gameObject));
            //    withinPerceptionRange = true;
            //}
            //else
            //{
            //    Debug.LogError("Perception.CheckLineOfSight(): " + c.name + " does not have CharacterInterface component!");
            //}
        }
        else
        {
            ChangeState(EnemyStates.Guard);
        }
    }

    private void ChangeState(EnemyStates state)
    {
        OnStateChanged(new StateChangedArgs() { enemyState = state });
    }
}
