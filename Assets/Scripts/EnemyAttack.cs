﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    private Transform player;
    [SerializeField]private EnemyAttackSettings enemyAttackSettings;
    private float timer = 0;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        timer += Time.deltaTime;
    }

   

    public void RangedAttack()
    {
        if (timer >= enemyAttackSettings.ShotInterval)
        {
            transform.LookAt(player);
            GameObject bullet = ObjectPooler.opSharedInstance.GetPooledObject(enemyAttackSettings.Bullet);
            bullet.transform.SetPositionAndRotation(transform.position + transform.forward, Quaternion.identity);

            enemyAttackSettings.ShootAt(transform, player);
            //// into SO it goes! (target)
            //bullet.GetComponent<Shot>().direction = (target.position - transform.position).normalized;

            ////// Randomize bullet movement so they shoot like Stormtroopers!
            ////if (Random.Range(0, 2) == 0)
            ////{
            ////    bullet.GetComponent<Shot>().direction += new Vector3(Random.Range(-0.015f, 0.015f), Random.Range(-0.02f, 0.02f), Random.Range(-0.05f, 0.05f));
            ////}
            //Quaternion bulletRotation = Quaternion.LookRotation(bullet.GetComponent<Shot>().direction);
            //bullet.transform.rotation = bulletRotation;
            timer = 0;
        }
    }

    public void MeleeAttack()
    {

    }

}
