﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAIMovement : MonoBehaviour {

    public Transform[] waypoints;
    public int curWaypoint = 0;
    bool ReversePath = false;
    NavMeshAgent navAgent;
    Vector3 Destination;
    float Distance;
    private Transform player;
    private float accuracy = 1;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        navAgent = GetComponent<NavMeshAgent>();
    }

    public void Patrol()
    {
    //    navAgent.isStopped = false;

        if (waypoints.Length == 0) { return; }
        if (Vector3.Distance(waypoints[curWaypoint].transform.position, transform.position) < accuracy)
        {
            curWaypoint++;
            if (curWaypoint == waypoints.Length)
            {
                curWaypoint = 0;
            }
        }
        navAgent.SetDestination(waypoints[curWaypoint].transform.position);
    }

    public void FollowPlayer()
    {
        navAgent.SetDestination(player.position);
    }

    public void StopAgent()
    {
        navAgent.isStopped = true;
    }

}
