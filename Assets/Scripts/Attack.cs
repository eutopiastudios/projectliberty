﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : Action {


    public override void Act(StateController controller)
    {
        FireAttack(controller);
    }

    private void FireAttack(StateController controller)
    {
        controller.enemyAttack.RangedAttack();
    }
}
