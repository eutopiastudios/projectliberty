﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UseableItem : MonoBehaviour
{

    public string m_tooltip;

    public abstract void Use(Vector3 userPosition);
}
