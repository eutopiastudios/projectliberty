﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Door : UseableItem
{
    public enum Axis
    {
        x,
        y,
        z
    }

    [Header("Opening speed in seconds")]
    public float m_openingSpeed;
    [Header("If the object can move in an inverted direction if opened from the \"other\" side ")]
    public bool openBothWays;
    [Header("If the movement can be interrupted halfway through")]
    public bool canInterrupt = true;
    public Axis m_bothWayAxisComparison;

    [Header("If canInterrupt is checked, max is within range (-360, 360)")]
    public bool m_rotates;
    public Vector3 m_openRotation;
    public Vector3 m_closedRotation;
    [Header("Position, is a bit wonky with openbothways it's a TODO for later...")]
    public bool m_moves;
    public Vector3 m_openPosition;
    public Vector3 m_closedPosition;

    [Space(10)]
    [Header("if object is locked and can't move")]
    public bool m_isLocked;

    [Header("Audio")]
    public bool m_playsAudio;
    public AudioSource m_audioSource;
    public AudioEvent m_openingSound;
    public AudioEvent m_closingSound;

    public delegate void OpenAction();
    public event OpenAction OnOpen;
    public delegate void ClosedAction();
    public event OpenAction OnClosed;

    private bool m_isClosed = true;
    private bool m_isOpenedInverse = false;

    private Coroutine m_currentRotationCoroutine;
    private Coroutine m_currentPositionCoroutine;

    private Vector3 m_originalRotation;
    private Vector3 m_originalPosition;

    [Header("Debug stuff")]
    public bool toggle;
    public bool inverseToggle;
    private bool prevToggle;

    private void Start()
    {
        m_originalRotation = transform.eulerAngles;
        m_originalPosition = transform.position;

        GetComponent<Rigidbody>().isKinematic = true;

        if (m_playsAudio && m_audioSource == null)
        {
            m_audioSource = GetComponent<AudioSource>();

            if (m_audioSource == null)
            {
                m_audioSource = gameObject.AddComponent<AudioSource>();
            }
        }
    }

    private void Update()
    {
        if (prevToggle != toggle)
        {
            if (toggle)
            {
                OpenDoor(false);
            }
            else
            {
                CloseDoor(m_isOpenedInverse);
            }

            prevToggle = toggle;
            inverseToggle = toggle;
        }
        else if (prevToggle != inverseToggle)
        {
            if (inverseToggle)
            {
                OpenDoor(true);
            }
            else
            {
                CloseDoor(m_isOpenedInverse);
            }

            prevToggle = inverseToggle;
            toggle = inverseToggle;
        }
    }

    public override void Use(Vector3 userPosition)
    {
        if(!canInterrupt && (m_currentPositionCoroutine != null || m_currentRotationCoroutine != null))
        {
            return;
        }

        if (!m_isLocked && m_isClosed)
        {
            m_isOpenedInverse = Foo(userPosition);
            OpenDoor(m_isOpenedInverse);
        }
        else
        {
            CloseDoor(m_isOpenedInverse);
        }
    }

    public bool Foo(Vector3 userPosition)
    {
        Vector3 userLocalPosition = transform.InverseTransformPoint(userPosition);

        switch (m_bothWayAxisComparison)
        {
            case Axis.x: return userLocalPosition.x > 0;
            case Axis.y: return userLocalPosition.y > 0;
            case Axis.z: return userLocalPosition.z > 0;
            default: return userLocalPosition.z > 0;
        }
    }

    public void OpenDoor(bool inverse)
    {
        m_isClosed = false;
        m_isOpenedInverse = inverse;

        if (m_rotates)
        {
            if (m_currentRotationCoroutine != null)
            {
                StopCoroutine(m_currentRotationCoroutine);
            }

            Vector3 start = m_originalRotation;
            Vector3 end = m_originalRotation + (inverse ? -m_openRotation : m_openRotation);

            m_currentRotationCoroutine = StartCoroutine(RotateDoor(start, end));
        }

        if (m_moves)
        {
            if (m_currentPositionCoroutine != null)
            {
                StopCoroutine(m_currentPositionCoroutine);
            }

            Vector3 start = m_originalPosition;
            Vector3 end = m_originalPosition + (inverse ? -m_openPosition : m_openPosition);

            m_currentPositionCoroutine = StartCoroutine(MoveDoor(start, end));
        }

        if (m_playsAudio && m_openingSound != null)
        {
            m_openingSound.Play(m_audioSource);
        }
    }

    public void CloseDoor(bool openedInverse)
    {
        m_isClosed = true;

        if (m_rotates)
        {
            if (m_currentRotationCoroutine != null)
            {
                StopCoroutine(m_currentRotationCoroutine);
            }

            Vector3 start = m_originalRotation + (openedInverse ? -m_openRotation : m_openRotation);
            Vector3 end = m_originalRotation;

            m_currentRotationCoroutine = StartCoroutine(RotateDoor(start, end));
        }

        if (m_moves)
        {
            if (m_currentPositionCoroutine != null)
            {
                StopCoroutine(m_currentPositionCoroutine);
            }

            Vector3 start = m_originalPosition + (openedInverse ? -m_openPosition : m_openPosition);
            Vector3 end = m_originalPosition;

            m_currentPositionCoroutine = StartCoroutine(MoveDoor(start, end));
        }

        if (m_playsAudio && m_closingSound != null)
        {
            m_closingSound.Play(m_audioSource);
        }
    }

    private IEnumerator RotateDoor(Vector3 startRotation, Vector3 endRotation)
    {
        float t = 0;

        if (m_currentRotationCoroutine != null)
        {

            t = SetRotT(startRotation, endRotation);
        }

        while (t < 1)
        {
            t += Time.deltaTime / m_openingSpeed;
            transform.eulerAngles = Vector3.Lerp(startRotation, endRotation, t);

            yield return null;
        }

        transform.eulerAngles = Vector3.Lerp(startRotation, endRotation, 1);
        m_currentRotationCoroutine = null;
    }

    private IEnumerator MoveDoor(Vector3 startPosition, Vector3 endPosition)
    {
        float t = 0;

        if (m_currentPositionCoroutine != null)
        {

            t = SetRotT(startPosition, endPosition);
        }

        while (t < 1)
        {
            t += Time.deltaTime / m_openingSpeed;
            transform.position = Vector3.Lerp(startPosition, endPosition, t);
            
            yield return null;
        }

        transform.position = Vector3.Lerp(startPosition, endPosition, 1);
        m_currentPositionCoroutine = null;
    }

    private float SetPosT(Vector3 startPosition, Vector3 endPosition)
    {
        float diffDistance = Vector3.Distance(startPosition, endPosition);
        float currentDistance = Vector3.Distance(transform.position, startPosition);

        return currentDistance / diffDistance;
    }

    private float SetRotT(Vector3 startRotation, Vector3 endRotation)
    {
        float diffDistance = Vector3.Distance(startRotation, endRotation);
        Vector3 currentRotation = transform.eulerAngles;

        if (currentRotation.x > m_openRotation.x * (m_isOpenedInverse ? -1 : 1))
        {
            currentRotation.x -= 360;
        }
        if (currentRotation.y > m_openRotation.y * (m_isOpenedInverse ? -1 : 1))
        {
            currentRotation.y -= 360;
        }
        if (currentRotation.z > m_openRotation.z * (m_isOpenedInverse ? -1 : 1))
        {
            currentRotation.z -= 360;
        }

        float currentDistance = Vector3.Distance(currentRotation, startRotation);
        return currentDistance / diffDistance; ;
    }
}
