﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorMove : MonoBehaviour {

    private bool Open;
    public bool locked = false;

    private float dist = 0.05f;
    public float moveDistance = 8.3f;
    public float duration = 1f;
    public float stayOpen = 5f;

    private Vector3 openedPos;
    private Vector3 closedPos;

    private AudioSource audioS;
    public AudioEvent openingSound;
    public AudioEvent openedSound;

    Coroutine closeDoorC;


    void Start()
    {
       closedPos = transform.position;
       openedPos = closedPos + new Vector3(0, moveDistance, 0);  
    }

    private IEnumerator MoveDoor(Vector3 newPos)
    {
        Vector3 origPos = transform.position;

        float elapsedTime  = 0;
        while (elapsedTime < duration)
        {
            transform.position = Vector3.Lerp(origPos, newPos, (elapsedTime / duration));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        transform.position = newPos;

        if (Open)
        {
            Invoke("CloseDoor", stayOpen);
        }
    }

    public void OpenDoor()
    {
        if(!locked)
        {
            StartCoroutine(MoveDoor(openedPos));
            Open = true;
            openingSound.Play(audioS);
        }
    }

    void CloseDoor()     // To be Invoked after StayOpen number of seconds
    {
        closeDoorC = StartCoroutine(MoveDoor(closedPos));
        Open = false;
        openingSound.Play(audioS);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            OpenDoor();
            if (closeDoorC != null)
            {
                StopCoroutine(closeDoorC);
            }       
        }
    }
}
