﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenGIFOMaticEmissive : MonoBehaviour {

    public Texture[] GifOMatic;
    public Texture[] EmissiveTextures;
    //public Material material;
    public int framespersec = 100;
    private int TextureNumber = 0;
    private int EmissionNumber = 0;
    private int count = 0;

    void Update ()
    {

        GetComponent<Renderer>().material.mainTexture = GifOMatic[TextureNumber];
        GetComponent<Renderer>().material.SetTexture("_EmissionMap", EmissiveTextures[EmissionNumber]);

        if (count == framespersec)
        {            
            TextureNumber++;
            EmissionNumber++;
            count = 0;
        }

        count++;

        if (TextureNumber == GifOMatic.Length)
        {
            TextureNumber = 0;
            EmissionNumber = 0;
        }  
     }
}
