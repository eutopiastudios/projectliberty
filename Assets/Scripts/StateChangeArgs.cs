﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StateChangedArgs : System.EventArgs
{
    public EnemyStates enemyState { get; set; }

}
