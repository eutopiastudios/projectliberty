﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PerceivedObjectData
{

    // this should actually be something that goes on characters, need to investigate how to use it...
    // will probably replace with scriptable object asset or sth

    public IEnemyPerceptionInput perceived;
    public float range;
    public GameObject gameObject;

    public PerceivedObjectData(IEnemyPerceptionInput p, float r, GameObject g)
    {
        perceived = p;
        range = r;
        gameObject = g;
    }
}
