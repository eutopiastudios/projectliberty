﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionParticles : MonoBehaviour {

    Vector3 playerPos;

	// Use this for initialization
	void Start () {
        playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        this.transform.position = playerPos;
	}
	
	// Update is called once per frame
	void Update () {
        transform.Rotate(0f, Time.deltaTime * 0.6f, 0f);
	}
}
