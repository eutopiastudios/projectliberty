﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColor : MonoBehaviour {

    EnemyPerception perception;

    private void Start()
    {
        perception = GetComponent<EnemyPerception>();
    }

    void Update()
    {
        if (perception.withinPerceptionRange)
        {
            GetComponent<Renderer>().material.SetColor("_Color", Color.red);
        }
        else
        {
            GetComponent<Renderer>().material.SetColor("_Color", Color.black);
        }
    }
}
